export const environment = {
	production: true,
	baseUrl: 'http://34.251.146.234:9000/api', // <Your API base url>
	imageUrl: 'http://34.251.146.234:9000/api/media/image-resize', // <Your API url for image resize>
	productUrl: 'http://54.76.207.234:4200/#/' // <Your store base url>
};
